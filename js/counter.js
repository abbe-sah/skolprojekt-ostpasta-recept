// Get references to the counter
const decreaseButton = document.getElementById('decrease');
const increaseButton = document.getElementById('increase');
const valueElement = document.getElementById('value');

// Get references to the ingredient amount
const pastaAmount = document.getElementById('Pasta').querySelector('#amount-pasta');
const graddeAmount = document.getElementById('Gradde').querySelector('#amount-gradde');
const mjolkAmount = document.getElementById('Mjolk').querySelector('#amount-mjolk');
const vetemjolAmount = document.getElementById('Vetemjol').querySelector('#amount-vetemjol');
const rivenOstAmount = document.getElementById('RivenOst').querySelector('#amount-rivenost');
const gronaArtorAmount = document.getElementById('GronaArtor').querySelector('#amount-artor');
const saltAmount = document.getElementById('Salt').querySelector('#amount-salt');
const persiljaAmount = document.getElementById('Persilja').querySelector('#amount-persilja');

// Define a function to update the ingredient amounts
function updateIngredientAmounts() {
  const counterValue = parseInt(valueElement.textContent);

  // Update the amounts of ingredients
  pastaAmount.textContent = counterValue * 75;
  graddeAmount.textContent = counterValue * 0.625;
  mjolkAmount.textContent = counterValue;
  vetemjolAmount.textContent = counterValue * 0.375;
  rivenOstAmount.textContent = counterValue;
  gronaArtorAmount.textContent = counterValue;
  saltAmount.textContent = counterValue * 0.125;
  persiljaAmount.textContent = counterValue * 0.125;
}

// Initialize the ingredient amounts with the normal values
updateIngredientAmounts();

// event listener for the decrease button
decreaseButton.addEventListener('click', () => {
  let counterValue = parseInt(valueElement.textContent);
  counterValue = Math.max(counterValue - 2, 2);
  valueElement.textContent = counterValue;
  updateIngredientAmounts();
});

// event listener for the increase button
increaseButton.addEventListener('click', () => {
  let counterValue = parseInt(valueElement.textContent);
  counterValue += 2;
  valueElement.textContent = counterValue;
  updateIngredientAmounts();
});
