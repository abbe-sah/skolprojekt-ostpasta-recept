const form = document.querySelector('#review-form');
const reviewsContainer = document.querySelector('#reviews');
const nameInput = document.querySelector('#name');
const ratingInput = document.querySelector('#rating');
const commentInput = document.querySelector('#comment');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  const name = nameInput.value;
  const rating = ratingInput.value;
  const comment = commentInput.value;

  const review = document.createElement('div');
  review.classList.add('review');
  review.innerHTML = `
    <h3>${name} - ${rating} Ärtor</h3>
    <p>${comment}</p>
  `;
  reviewsContainer.appendChild(review);

  nameInput.value = '';
  ratingInput.value = '';
  commentInput.value = '';
});
