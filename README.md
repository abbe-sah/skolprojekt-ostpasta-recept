# Skolprojekt-OstPasta-Recept

<img src="https://cdn.imgpaste.net/2023/04/26/KHPVAS.png" alt="KHPVAS.png" border="0" />
<hr>

## Layout inspiration från arla.se receptsida

<img src="https://cdn.imgpaste.net/2023/04/26/KHPJ4P.png" alt="KHPJ4P.png" border="0" />
<hr>

## Färg inspiration från Nike

<img src="https://cdn.imgpaste.net/2023/04/26/KHPysI.jpg" alt="KHPysI.jpg" border="0" />

<hr>

## Validator.w3.org Check
<img src="https://cdn.imgpaste.net/2023/04/26/KHPZ7p.png" alt="KHPZ7p.png" border="0" />
<hr>

## Bild på hela hemsidan med alla funktioner.Byggd i HTML, CSS, JS.

<a href="https://www.imgpaste.net/image/KHPbON"><img src="https://cdn.imgpaste.net/2023/04/26/KHPbON.png" alt="KHPbON.png" border="0" /></a>

<hr>

## Responsive layout som anpassar sig efter skärmens storlek.
<img src="https://cdn.imgpaste.net/2023/04/26/KHPpg6.png" alt="KHPpg6.png" border="0" />
<hr>

## Utveckling
Bygga en enkel backend i spring boot.
Hämta och skriva rescensioner med json till ett API endpoint.

